import './App.css'
import MyForm from './form.tsx'


function App() {
  return (
    <>
      <MyForm />
    </>
  )
}

export default App
