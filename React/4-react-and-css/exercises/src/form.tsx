import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';

function MyForm() {
    return (
        <Form>
            <Form.Group className="mb-3">
                <Form.Label>Email address</Form.Label>
                <Form.Control size="lg" placeholder="Enter email" />
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Password</Form.Label>
                <Form.Control size="lg" type="password" placeholder="Password" />
            </Form.Group>
            <Form.Group className="mb-3">
                <Form.Check type="checkbox" label="Check me out" />
            </Form.Group>
            <div className="d-grid gap-2 pb-2">
                <Button variant="primary" type="submit">
                    Submit
                </Button>
                <Button variant="light" type="reset">
                    Reset form
                </Button>
            </div>
            <Form.Check
                type="radio"
                label="Radio 1"
                name="radioGroup"></Form.Check>
            <Form.Check
                type="radio"
                label="Radio 2"
                name="radioGroup"></Form.Check>
        </Form>
    );
}

export default MyForm;