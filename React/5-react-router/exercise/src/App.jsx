import { useState } from 'react'
import './App.css'
import {
  BrowserRouter as Router,
  Routes, Route, Link
} from "react-router-dom";


const App = () => {
  //const [page, setPage] = useState('home')


  const Home = () => (
    <div> <h2>Hello and welcome!</h2> </div>
  )

  const About = () => (
    <div> <h2>This is for a React exercise</h2> </div>
  )

  const Notes = () => (
    <div> <h2>Notes are here</h2> </div>
  )

  const Links = () => (
    <div> <h2>Links</h2>
      <div><a href="https://fi.wikipedia.org/wiki/Wikipedia:Etusivu">
        Wikipedia
      </a></div>
      <div>
        <a href="https://www.hs.fi/">
          Hesari
        </a>
      </div>
      <div>
        <a href="https://www.ilmatieteenlaitos.fi/saa/jyv%C3%A4skyl%C3%A4">
          Jyväskylän sää
        </a>
      </div>
    </div>
  )

  const padding = { padding: 5 }

  return (
    <Router>
      <div>
        <Link style={padding} to="/">Home</Link>
        <Link style={padding} to="/about">About</Link>
        <Link style={padding} to="/links">Links</Link>
        <Link style={padding} to="/notes">Notes</Link>
      </div>

      <Routes>
        <Route path="/about" element={<About />} />
        <Route path="/links" element={<Links />} />
        <Route path="/" element={<Home />} />
        <Route path="/notes" element={<Notes />} />
      </Routes>
    </Router>
  )

}



export default App
