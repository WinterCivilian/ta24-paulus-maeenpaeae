

const Year = () => {
    const currentYear = new Date().getFullYear();
    return (<h2>It is year {currentYear}</h2>);
}

export default Year;