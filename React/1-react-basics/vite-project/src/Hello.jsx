
import Year from './Year.jsx'

const Hello = (props) => {
    return (
        <div>
            <h1>Hello {props.name}</h1>
            <Year currentYear={props.currentYear} />
        </div>
    );
}

export default Hello;