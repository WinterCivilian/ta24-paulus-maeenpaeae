
import Hello from './Hello.jsx'
import Year from './Year.jsx'

const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];


const App = () => {
  const currentYear = <Year />;
  return (
    <div>
      <Hello name='Mikko' currentYear={currentYear} />
      <Hello name='Hannes' currentYear={currentYear} />
      <Hello name='Taavi' currentYear={currentYear} />
      <div>
        {namelist.map((personName, index) => (
          <li key={index} style={{
            fontStyle: index % 2 !== 0 ? 'italic' : 'normal',
            fontWeight: index % 2 === 0 ? 'bold' : 'normal'
          }}>{personName}</li>
        ))}
      </div>
    </div >
  )
}

export default App;