import './App.css'
import Greetings from './Greetings.jsx'
import r2d2 from './r2d2.jpg';
import Planets from './Planets.jsx';


const planetList = [
  { name: "Hoth", climate: "Ice" },
  { name: "Tattooine", climate: "Desert" },
  { name: "Alderaan", climate: "Temperate" },
  { name: "Mustafar", climate: "Volcanic", }
];


function App() {
  const fullName = "Matti Meikäläinen";
  const age = 25;
  return (
    <>
      <h1>Assignment 1</h1>
      <div>
        <Greetings name={fullName} age={age} />
      </div>
      <h1 className="assignment2">Assignment 2</h1>
      <div className="img-container">
        <img src={r2d2} alt="R2D2"></img>
        <p className="R2talk">Hello I am R2D2!</p>
        <p className="R2beeps">BeeYoop BeeDeepBoom Weeop DEEpaEEya!</p>
      </div>
      <h1>Assignment 3</h1>
      <div>
        <Planets planetlist={planetList} />
      </div>
    </>
  )
}

export default App
