function Planets(props) {
    return (
        <table>
            {props.planetlist.map(planet => <p key={planet.name}>{planet.name} {planet.climate}</p>)}
        </table>
    );
}

export default Planets;