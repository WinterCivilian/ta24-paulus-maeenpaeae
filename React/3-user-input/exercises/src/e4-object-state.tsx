import { useState } from 'react'

function ObjectState() {
    //const [savedValue, setSavedValue] = useState('');

    const [state, setState] = useState({
        counter: 0,
        text: '',
    });

    const onTextChange = (event) => {

        const newState = { ...state, text: event.target.value };
        setState(newState);
        //setState(event.target.value)
    }
    function handleClick() {
        const newState = { ...state, counter: state.counter + 1, text: '' };
        setState(newState);
    }

    return (
        <div>
            <button onClick={handleClick}>
                {state.counter}
            </button>
            <input value={state.text} onChange={onTextChange} />
            <p>{state.text}</p>
            <p>{state.counter}</p>
        </div >
    );


}

export default ObjectState