function Counter(props) {

    const onCounterClick = () => { props.incrementCounter(); };

    const onDeleteClick = () => {
        props.deleteCounter();
    };

    return (
        <ul key={props.id}>
            <button onClick={() => onCounterClick()} >
                {props.counter}
            </button>
            <button onClick={() => onDeleteClick()}>Delete</button>
        </ul>);
}

export default Counter;