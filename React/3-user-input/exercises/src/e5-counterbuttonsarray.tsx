import { useState } from 'react'

function CounterButton(props) {
    return (
        <button onClick={props.onClick}>
            {props.value}
        </button>
    );
}

function CounterButtonsArray() {
    const [counters, setCounters] = useState([0, 0, 0, 0]);

    function handleClick(index: number) {
        console.log("handleclick");
        const newCounters = counters.map((c, i) => {
            if (i === index) {
                // Increment the clicked counter
                return c + 1;
            } else {
                // The rest haven't changed
                return c;
            }
        });
        setCounters(newCounters);
    }

    //<CounterButton onClick={() => handleClick(0)} value={counters[0]} /> //e5
    return (
        <div>
            {counters.map((value, index) => (
                <CounterButton key={index} onClick={() => handleClick(index)} value={value} />))}
        </div>)
}

export default CounterButtonsArray;