import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid';

//import Form from './e3-form-input.tsx'
//import ObjectState from './e4-object-state.tsx';
//import CounterButtonsArray from './e5-counterbuttonsarray'
//import CounterButtonsItem from './e7-array-of-objects-state';
import Counter from './e8-Counter';

function App() {
  /*const [text, setText] = useState('')
  const [savedValue, setSavedValue] = useState('');

  const onNameChange = (event) => {
    setText(event.target.value)
  }

  function handleClick() {
    setText('');
    setSavedValue(text);
  }

  <h1 className='header'>Your string is {savedValue}</h1>
  <input value={text}
    onChange={onNameChange} />
  <button onClick={handleClick}>submit</button>
  */

  //<Form /> //e3
  //<ObjectState /> //e4
  //<CounterButtonsArray /> //e5
  //<CounterButtonsItem /> //e7 

  const [items, setItems] = useState([
    { id: 1, counter: 0 },
    { id: 2, counter: 0 },
    { id: 3, counter: 0 },
    { id: 4, counter: 0 }
  ]);

  function incrementCounter(id) {
    const newItems = items.map((item) => {
      if (item.id === id) {
        return { ...item, counter: item.counter + 1 };
      } else {
        return item;
      }
    });
    setItems(newItems);
  }

  function addNewCounter() {
    setItems([...items, { id: uuidv4(), counter: 0 }]);
  }

  function deleteCounter(id) {
    setItems(items.filter(item => item.id !== id));
  }

  //const [searchText, setSearchText] = useState('');
  const [minimumCounter, setMinimumCounter] = useState(0);

  const filteredItems = items.filter(item =>
    item.counter >= (minimumCounter)
  )

  const [showAll, setShowAll] = useState(true);

  const handleCheckboxChange = () => {
    setShowAll(!showAll);
  };


  return (
    <>
      <input type="checkbox" checked={setShowAll}
        onChange={handleCheckboxChange}></input>
      < div className='App' >
        {filteredItems.map((item) => (<Counter key={item.id} counter={item.counter}
          incrementCounter={() => incrementCounter(item.id)}
          deleteCounter={() => deleteCounter(item.id)} />))}
        <button onClick={() => addNewCounter()} >Add a new button!</button>
      </div >
      <div>{(showAll && <input type='number' value={minimumCounter}
        onChange={e => setMinimumCounter(Number(e.target.value))} />)}</div>
    </>)
}

export default App;