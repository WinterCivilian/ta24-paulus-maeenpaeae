import { useState } from 'react'

function Form() {
    const [text, setText] = useState('')
    const [savedValue, setSavedValue] = useState('');

    const onNameChange = (event) => {
        setText(event.target.value)
    }

    function handleSubmit(event) {
        event.preventDefault();
        setText('');
        setSavedValue(text);
    }
    return (
        <form onSubmit={handleSubmit}>
            <h1 className='header'>Your string is {savedValue}</h1>
            <input value={text}
                onChange={onNameChange} />
            <button type="submit">submit</button>
        </form>
    );
}

export default Form;