import { useState } from 'react'

function CounterButton(props) {
    return (
        <button onClick={props.onClick}>
            {props.value}
        </button>
    );
}

function CounterButtonsItem() {

    const [items, setItems] = useState([
        { id: 1, counter: 0 },
        { id: 2, counter: 0 },
        { id: 3, counter: 0 },
        { id: 4, counter: 0 }
    ]);


    function handleClick(index: number) {
        const newItems = items.map((item, i) => {
            if (i === index) {
                // Increment the clicked counter
                return { ...item, counter: item.counter + 1 };
            } else {
                // The rest haven't changed
                return item;
            }
        });
        setItems(newItems);
    }

    return (
        <ul>
            {items.map((item, index) => (<CounterButton key={item.id} onClick={() =>
                handleClick(index)} value={item.counter} />))}
        </ul>);
}

export default CounterButtonsItem;