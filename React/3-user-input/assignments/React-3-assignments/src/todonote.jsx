import './todonote.css';

function TodoNote({ toggleCompletion, id, removeTodo, complete, text }) {

    //(props)

    const handleClick = () => {
        toggleCompletion(id);
    };

    const handleDelete = () => {
        removeTodo(id);
    }

    return (
        <div className={complete ? 'done' : 'notDone'}>
            <p>{text}</p>
            <input type="checkbox"
                checked={complete}
                onChange={handleClick} />
            <button onClick={handleDelete}>X</button>
        </div >

    )
}

export default TodoNote;