import { useState } from 'react'
import TodoNote from './todonote'
import { v4 as uuidv4 } from 'uuid';
import InputForm from './inputform'
import './App.css'

function App() {

  const [todos, setTodos] = useState([
    { id: 1, text: 'Buy potatoes', complete: false },
    { id: 2, text: 'Make food', complete: false },
    { id: 3, text: 'Exercise', complete: false },
    { id: 4, text: 'Do the dishes', complete: false },
    { id: 5, text: 'Floss the teeth', complete: false },
    { id: 6, text: 'Play videogames', complete: true },
  ])

  function toggleCompletion(id) {
    const newTodos = todos.map((todoItem) => {
      if (todoItem.id === id) {
        return { ...todoItem, complete: !todoItem.complete };
      } else {
        return todoItem;
      }
    });
    setTodos(newTodos);
  }

  function removeTodo(id) {

    setTodos(todos.filter(todo => todo.id !== id));
  }

  function addTodo(text) {
    setTodos([...todos, { id: uuidv4(), text: text, complete: false }]);
  }

  return (
    <>
      {todos.map((todo) => (
        <TodoNote
          key={todo.id} complete={todo.complete}
          text={todo.text}
          toggleCompletion={() => toggleCompletion(todo.id)}
          removeTodo={() => removeTodo(todo.id)}
        />))}
      <InputForm addTodo={addTodo} />
    </>
  )
}
export default App