import { useState } from 'react'
import './inputform.css'

function InputForm(props) {

    const [newTodo, setNewTodo] = useState("");

    const onSubmit = (event) => {
        event.preventDefault();
        props.addTodo(newTodo);
        setNewTodo("");
    };

    return (
        <form onSubmit={onSubmit}>
            <input
                className="inputField"
                type="text"
                placeholder='Add a new note'
                value={newTodo}
                onChange={(event) => setNewTodo(event.target.value)} />
            <input type="submit" value="Submit" className="inputButton" />
        </form>
    );

}

export default InputForm;