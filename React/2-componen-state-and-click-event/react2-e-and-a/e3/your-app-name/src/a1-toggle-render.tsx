
function HideA1(props) {

    function handleClick() {
        props.setHide(prevClicked => !prevClicked);
    }

    return (
        <div>
            <button onClick={handleClick}> Click me
            </button>
        </div>
    );
}

export default HideA1;