
function ClickHide(props) {

    function handleClick() {
        props.setClicked(prevClicked => !prevClicked);
    }

    return (
        <div>
            <button onClick={handleClick}> Click me
            </button>
        </div>
    );
}

export default ClickHide;