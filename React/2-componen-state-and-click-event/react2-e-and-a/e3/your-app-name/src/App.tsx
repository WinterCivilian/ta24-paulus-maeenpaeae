import { useState } from 'react'
import Counter from './e3-counter'
import ClickHide from './e6-hide-unhide-counters';
import HideA1 from './a1-toggle-render';
import Buttons from './buttons';
import Bingo from './bingo';

function App() {
  const [sum, setSum] = useState(0);
  const [clickState, setClicked] = useState(false);
  const [hideState, setHide] = useState(false);
  const [remove, setRemoved] = useState(false);
  const names = [
    "Anakin Skywalker",
    "Leia Organa",
    "Han Solo",
    "C-3PO",
    "R2-D2",
    "Darth Vader",
    "Obi-Wan Kenobi",
    "Yoda",
    "Palpatine",
    "Boba Fett",
    "Lando Calrissian",
    "Jabba the Hutt",
    "Mace Windu",
    "Padmé Amidala",
    "Count Dooku",
    "Qui-Gon Jinn",
    "Aayla Secura",
    "Ahsoka Tano",
    "Ki-Adi-Mundi",
    "Luminara Unduli",
    "Plo Koon",
    "Kit Fisto",
    "Shmi Skywalker",
    "Beru Whitesun",
    "Owen Lars"
  ];

  return (
    <>
      <div>
        {clickState == false ? (
          <h2>
            <Counter setSum={setSum} />
            <Counter setSum={setSum} />
            <Counter setSum={setSum} />
            <Counter setSum={setSum} />
            <p>{sum}</p>.</h2>
        ) : (
          <h2>Hidden!</h2>
        )}
      </div >
      <div>
        <ClickHide setClicked={setClicked} />
      </div>
      <h1>Assignment 1</h1>
      <div>
        <HideA1 setHide={setHide} />
      </div>
      <div>
        {hideState == false ? (
          <h2>Hello hello!</h2>
        ) : (
          <h2></h2>
        )}
      </div >
      <h1>Assignment 2</h1>
      <div>
        <Buttons number={5} remove={remove} setRemoved={setRemoved} />
      </div>
      <h1>Assignment 3</h1>
      <Bingo names={names} />
    </>
  )
}

export default App