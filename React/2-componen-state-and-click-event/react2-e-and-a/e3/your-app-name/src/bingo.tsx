import React, { useState } from 'react';
import './bingo.css';

const Bingo = ({ names }) => {

    const [clickedButtons, setClickedButtons] = useState(new Array(names.length).fill(false));

    const handleClick = (index) => {

        const newClickedButtons = [...clickedButtons];
        // Toggle the value at the clicked index
        newClickedButtons[index] = !newClickedButtons[index];
        // Update the state with the new array
        setClickedButtons(newClickedButtons);

        checkBingo(newClickedButtons);
    }

    function checkBingo(a) {
        const isEveryFifthTrue = a.every((value: boolean, index: number) =>
            (index + 1) % 5 === 0 && value);

        console.log(isEveryFifthTrue);
    }


    return (
        <div className="bingo">
            {names.map((name: string, index: number) => (
                <button
                    className={`grid-unit ${clickedButtons[index] ? 'green' : 'red'}`}
                    onClick={() => handleClick(index)}
                    key={index}>{name}
                </button>
            ))}
        </div>
    );
};

export default Bingo;