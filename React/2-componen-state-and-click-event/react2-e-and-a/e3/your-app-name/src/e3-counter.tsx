import React, { useState } from 'react';


function Counter(props) {
    const [count, setCount] = useState(0)

    function handleClick() {
        setCount(count + 1);
        props.setSum(prevSum => prevSum + 1)
    }

    return (
        <div>
            <button onClick={handleClick}>
                {count}
            </button>
        </div >
    );
}

export default Counter;