
function DisapperingButton(props) {

    function handleClick() {
        props.setRemoved(true);
    }

    return (
        <div>
            {props.remove ? null : (
                <button onClick={handleClick}> button {props.number}</button>
            )}
        </div>
    );
}

export default DisapperingButton;