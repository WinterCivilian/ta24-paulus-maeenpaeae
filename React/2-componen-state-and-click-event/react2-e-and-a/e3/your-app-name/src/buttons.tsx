import DisapperingButton from "./disappearingbutton";


function Buttons(props) {

    const buttons = [];
    for (let i = 0; i < props.number; i++) {
        buttons.push(<DisapperingButton key={i} number={i + 1} remove={props.remove}
            setRemoved={props.setRemoved} />);
    }

    return (
        <div>
            {buttons}
        </div>)
}

export default Buttons;