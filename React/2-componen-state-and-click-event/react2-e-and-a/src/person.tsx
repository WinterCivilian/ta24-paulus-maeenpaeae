
const Person = (props) => {
    return (
        <div>
            <li>{props.name} is {props.age} years old </li>
        </div>
    );
}

/*
     <ul>
    {people.map((person, index: number) => (
    <li key={index}>{person.name} is {person.age} years old </li>
    ))}
    </ul>
*/

export default Person;