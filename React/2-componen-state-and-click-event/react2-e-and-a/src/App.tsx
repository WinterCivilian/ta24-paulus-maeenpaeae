import './App.css'
import Family from './family.tsx'

export const people = [
  { name: 'Alice', age: 30 },
  { name: 'Bob', age: 35 },
  { name: 'Charlie', age: 40 },
  { name: 'Donna', age: 45 }
];

export function App() {

  return (
    <>
      <h1>Exercise 1</h1>
      <Family />
    </>
  )
}
