import { people } from './App.tsx'
import Person from './person.tsx'

const Family = () => {
    return (
        <div>
            <h3>Family Members:</h3>
            <ul>
                {people.map((person, index) => (
                    <Person key={index} name={person.name} age={person.age} />
                ))}
            </ul>
        </div>
    );
}

export default Family;