let seconds = -1;
const counter = document.getElementById("seconds");
let minutes = 0;
let minuteCounterActivated = false;

function secondCounter() {
    seconds += 1;

    if (seconds === 60) {
        seconds = 0;
        minutes += 1;
        minuteCounterActivated = true;
    }
    if (minuteCounterActivated) {
        counter.innerText = minutes + " minutes " + seconds + " seconds.";
    } else {
        counter.innerText = seconds + " seconds.";
    }
}

let recall = setInterval(secondCounter, 1000);