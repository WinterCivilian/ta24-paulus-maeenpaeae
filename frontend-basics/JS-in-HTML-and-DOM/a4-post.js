const postAdder = document.getElementById("submitbutton");


postAdder.addEventListener("click", function (event) {
    event.preventDefault(); // Prevent page from reloading on click
    const posts = document.getElementById("posts");
    addPost();
});

function addPost() {
    const input = document.getElementById("userInput").value;
    const userName = document.getElementById("userName").value;

    if (input.length == 0) {
        alert("Error! Input can't be empty!");
        return;
    }


    if (userName.length == 0) {
        alert("Error! Name can't be empty!");
        return;
    }
    const postDiv = document.createElement("div");
    postDiv.classList.add("post");

    //Post input
    const pTag = document.createElement("p");
    pTag.innerHTML = input;

    //Username input
    const uTag = document.createElement("p");
    uTag.innerHTML = userName;

    //Delete button
    const deleteButton = document.createElement("button");
    deleteButton.textContent = "Delete";
    deleteButton.classList.add("delete-button");
    deleteButton.addEventListener("click", function () {

        postDiv.remove();
    });

    postDiv.appendChild(uTag);
    postDiv.appendChild(pTag);
    postDiv.appendChild(deleteButton);

    document.getElementById("posts").appendChild(postDiv);
}