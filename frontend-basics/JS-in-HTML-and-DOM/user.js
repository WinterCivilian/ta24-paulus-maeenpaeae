function load() {
    console.log("Welcome, [username]!");
}

window.onload = load;

function inputName() {
    /*console.log("Button clicked!");*/
    var data = document.getElementById("name");
    console.log("Welcome, " + data.value + "!");
}

const nameChanger = document.getElementById("mybutton");
nameChanger.addEventListener("click", function (event) {
    event.preventDefault(); // Prevent page from reloading on click
    inputName();
});