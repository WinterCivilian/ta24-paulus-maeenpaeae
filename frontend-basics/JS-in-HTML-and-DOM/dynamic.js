//1)
const studentAdder = document.getElementById("studentButton");
studentAdder.addEventListener("click", function (event) {
    event.preventDefault(); // Prevent page from reloading on click
    const posts = document.getElementById("someDiv");
    if (posts.childNodes.length <= 0) {
        addStudents();
    }
    else {
        deleteStudents()
    }
});

function addStudents() {
    for (let i = 0; i < students.length; i++) {
        const pTag = document.createElement("p");
        pTag.innerHTML = `Name: ${students[i].name} score: ${students[i].score}`;
        document.getElementById("someDiv").appendChild(pTag);
    }
}

function deleteStudents() {
    const posts = document.getElementById("someDiv");

    while (posts.firstChild) {
        posts.removeChild(posts.firstChild);
    }
}


const students = [{ name: "Sami", score: 24.75 },
{ name: "Heidi", score: 20.25 },
{ name: "Jyrki", score: 27.5 },
{ name: "Helinä", score: 26.0 },
{ name: "Maria", score: 17.0 },
{ name: "Yrjö", score: 14.5 }];
