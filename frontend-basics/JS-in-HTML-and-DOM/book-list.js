const books = [{ name: "Dune", pagecount: 412 }, { name: "The Eye of the World", pagecount: 782 }];


const bookAdder = document.getElementById("bookbutton");
bookAdder.addEventListener("click", function (event) {
    event.preventDefault(); // Prevent page from reloading on click
    const posts = document.getElementById("bookrow");
    for (let i = 0; i < books.length; i++) {
        const pTag = document.createElement("p");
        pTag.innerHTML = `Name: ${books[i].name} page count: ${books[i].pagecount}`;
        document.getElementById("bookrow").appendChild(pTag);
    }
});

