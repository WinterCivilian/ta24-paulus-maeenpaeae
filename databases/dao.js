import { v4 as uuidv4 } from 'uuid'
import { executeQuery } from './db.js'
import * as dao from './dao.js'
//import * as queries from './queries.js'
//const result = await executeQuery(queries.insertProduct, params);


const insertProduct = async (product) => {
    const id = uuidv4();
    const params = [id, ...Object.values(product)];
    console.log(`Inserting a new product ${params[0]}...`); //, params  VARCHAR(100)
    //const query = "INSERT INTO products (name, price) VALUES ('VASARA', 30);"
    console.log(params);
    console.log(product.name);
    const query = `INSERT INTO products (name, price) 
    VALUES ('${product.name}', ${product.price});`


    const result = await executeQuery(query);
    console.log(`New product ${id} inserted successfully.`);
    return result;
}

const updateProduct = async (product) => {
    const params = [product.name, product.price, product.id]
    console.log(`Updating a product ${params[0]} ...* `)

    const query = `UPDATE products 
    SET name = $1,
        price = $2,
        id = $3
    WHERE id = 100;`

    const result = await executeQuery(query, params);
    console.log(`Product ${product} updated successfully.`)
    return result;
}


const findAll = async () => {
    console.log('Requesting for all products ... ')

    const query = `SELECT * FROM products;`

    const result = await executeQuery(query) //dao.findAll
    console.log(`Found ${result.rows.length} products.`)
    return result;
}

const findOne = async (id) => {

    console.log(id);
    console.log(`Requesting a product with id: ${id}`)

    const query = `SELECT * FROM products WHERE id = $1;`
    const result = await executeQuery(query, [id])
    console.log(`Found $(result.rows.length} products.`)
    return result
}


const deleteById = async (id) => {
    console.log(`Deleting product with id: ${id} `)
    const params = [id];

    const query = `DELETE FROM products WHERE id = $1;`
    const result = await executeQuery(query, params);
    return result;
}

export { insertProduct, findAll, findOne, updateProduct, deleteById }

