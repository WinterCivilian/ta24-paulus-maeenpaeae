import express from "express";
import { createProductsTable } from "./db.js";
import { insertProduct, updateProduct, findAll, findOne, deleteById } from "./dao.js";


const server = express();

createProductsTable();

const testi = {
    name: 'testi',
    price: 30,
    id: 4
}

//insertProduct(testi);
//updateProduct(testi);

//findAll();

//findOne(testi.id);

deleteById(testi.id);

const { PORT } = process.env;
server.listen(PORT, () => {
    console.log("Products API listening to port", PORT);
});


