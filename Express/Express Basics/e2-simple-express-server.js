import express from 'express'

const server = express()

server.listen(3000, () => {
    console.log('Listening to port 3000')
})


server.get("/", (request, response) => {
    response.send("Hello World!")
})


server.get('/endpoint2', (req, res) => {
    res.send('You found the end point!')
})
