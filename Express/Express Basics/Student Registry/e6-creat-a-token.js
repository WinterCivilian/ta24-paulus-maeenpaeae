import 'dotenv/config'
import jwt from 'jsonwebtoken'

const user = { username: "Herkkules", height: "2m" }

const payload = user;
const secret = process.env.SECRET
const options = { expiresIn: '15min' }

const token = jwt.sign(payload, secret, options)
console.log(token)
