import 'dotenv/config'
import jwt from 'jsonwebtoken'

const token = process.argv[2];
const secret = process.env.SECRET;

jwt.verify(token, secret, (err, decoded) => {
    if (err) {
        console.error('Token verification failed:', err);
    } else {
        console.log('Token decoded successfully:', decoded);
    }
});