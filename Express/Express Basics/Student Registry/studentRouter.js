import express from 'express'
import students from './student-registry.js'
import { errorUnknown, logger, validateStudent, putStudent } from './middleware.js';

const router = express.Router()

const findStudent = (id) => {
    return students.find(student => student.id == id);
};


router.get('/', (req, res) => {
    res.send("Hello World!  ")
})

router.get('/route', (req, res) => {
    res.send('OK')
})

router.get('/students', (req, res) => {
    const studentIds = students.map(student => student.id);
    res.send(studentIds);
})


router.post("/student", validateStudent, (req, res) => {
    console.log("POST request init!");
    console.log(req.body);
    res.status(201).send("");
    students.push(req.body);
})

router.get("/student/:id", (req, res) => {
    const student = findStudent(req.params.id);
    if (student) {
        const studentJSON = JSON.stringify(student);
        console.log(studentJSON);
        res.send(studentJSON);
    } else {
        res.status(404).send("Error, no such student");
    }
});

router.put("/student/:id", putStudent, (req, res) => {
    const student = findStudent(req.params.id);
    if (student) {
        Object.assign(student, req.body);
        console.log(students);
        res.status(204).send("");
    } else {
        res.status(404).send("Error, no such student");
    }
});

router.delete("/student/:id", putStudent, (req, res) => {
    const id = req.params.id;
    const index = students.findIndex(student => student.id == id);
    if (index !== -1) {
        students.splice(index, 1);
        console.log(students);
        res.status(204).send("");
    } else {
        res.status(404).send("Error: Student not found");
    }
})

export default router