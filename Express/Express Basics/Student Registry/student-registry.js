import express from 'express'
import url from 'url';

const server = express()

const adr = 'http://localhost:3005/students';
const q = url.parse(adr, true);

server.listen(3005, () => {
    console.log('Listening to port 3005')
})


server.use(express.json())
server.use(express.urlencoded({ extended: false }))

server.get("/", (request, response) => {
    response.send("Hello World!");
})

const students = [{ name: "Joona", id: 1, email: "joona@student.fi" },
{ name: "Jonne", id: 2, email: "jonne@student.fi" }];

import { errorUnknown, logger, validateStudent, putStudent } from './middleware.js';



server.use("/", logger)


server.get('/students', (req, res) => {
    const studentIds = students.map(student => student.id);
    res.send(studentIds);
})


server.post("/student", validateStudent, (req, res) => {
    console.log("POST request init!");
    console.log(req.body);
    res.status(201).send("");
    students.push(req.body);
})


const findStudent = (id) => {
    return students.find(student => student.id == id);
};

server.get("/student/:id", (req, res) => {
    const student = findStudent(req.params.id);
    if (student) {
        const studentJSON = JSON.stringify(student);
        console.log(studentJSON);
        res.send(studentJSON);
    } else {
        res.status(404).send("Error, no such student");
    }
});



server.put("/student/:id", putStudent, (req, res) => {
    const student = findStudent(req.params.id);
    if (student) {
        Object.assign(student, req.body);
        console.log(students);
        res.status(204).send("");
    } else {
        res.status(404).send("Error, no such student");
    }
});

server.delete("/student/:id", putStudent, (req, res) => {
    const id = req.params.id;
    const index = students.findIndex(student => student.id == id);
    if (index !== -1) {
        students.splice(index, 1);
        console.log(students);
        res.status(204).send("");
    } else {
        res.status(404).send("Error: Student not found");
    }
})

server.use(errorUnknown);
