import argon2 from 'argon2'
import express from 'express'


const router = express.Router()

let users = [];

router.post("/register", (req, res) => {
    const newUser = req.body.username;
    const newPassword = argon2.hash(req.body.password)
        .then(result => {
            const user = { username: newUser, password: result }
            users.push(user);
            console.log(users);
            console.log("POST request init!");
            res.status(201).send("Registered as " + user.username);
        })
})

export default router