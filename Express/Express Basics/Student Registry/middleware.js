export const logger = (req, res, next) => {
    let time = new Date(Date.now());
    console.log('Current time: ' + time);
    console.log('Used method: ' + req.method);
    console.log('Used name and path ' + req.hostname + req.path);
    /*if (req.body) {
        console.log('Body: ' + req.body);
    }*/
    next();
}


export const errorUnknown = (req, res) => {
    console.log("errorUnknown");
    res.status(404).send("Error, page can't be found");
}

export const validateStudent = (req, res, next) => {
    const { name, id, email } = req.body
    if (!name || !id || !email) {
        return res.status(400).send('Missing or invalid parameters validate')
    }
    next()
}

export const putStudent = (req, res, next) => {
    const { name, email } = req.body
    if (!name && !email) {
        return res.status(400).send('Missing name and email');
    }
    next()
}
