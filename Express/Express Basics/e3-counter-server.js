import express from 'express'

const server = express()

server.listen(3001, () => {
    console.log('Listening to port 3001')
})


server.get("/", (request, response) => {
    response.send("Hello World!")
})

let counter = 0;

server.get('/counter', (req, res) => {
    counter++;
    res.send('You have visited this site ' + counter + ' times')
})
