import http from 'http'

const server = http.createServer((_req, res) => {
    res.write('Hello world!')
    res.end()
})
const port = 5005

server.listen(port)
console.log('HTTP Server listening port', port)
