import express from 'express'

const server = express()

server.listen(3002, () => {
    console.log('Listening to port 3002')
})


server.get("/", (request, response) => {
    response.send("Hello World!")
})

let counter = 0;
let johncounter = 0;
let janecounter = 0;

server.get('/counter/:id', (req, res) => {
    counter++;
    const id = req.params.id
    const info = data.find(item => item.id === id)
    if (id == ":John") {
        johncounter++;
        res.send('John has visited this site ' + johncounter + ' times');
    }
    else if (id == ":Jane") {
        janecounter++;
        res.send('Jane has visited this site ' + janecounter + ' times');
    }
})


server.get('/counter', (req, res) => {
    counter++;

    if (req.query.counter) {
        res.send('You have visited this site ' + req.query.counter + ' times')
    }
    res.send('You have visited this site ' + counter + ' times')

})

const data = [
    { id: 1, name: 'John' },
    { id: 2, name: 'Jane' }
]