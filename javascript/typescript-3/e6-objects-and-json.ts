import fs from "fs"


const library = [
    {
        author: 'David Wallace',
        title: 'Infinite Jest',
        readingStatus: false,
        id: 1,
    },
    {
        author: 'Douglas Hofstadter',
        title: 'Gödel, Escher, Bach',
        readingStatus: true,
        id: 2,
    },
    {
        author: 'Harper Lee',
        title: 'To Kill A Mockingbird',
        readingStatus: false,
        id: 3,
    },
];

function saveToJSON() {
    fs.writeFileSync("library.json", JSON.stringify(library), "utf8");
}

function loadFromJSON() {

    const new_library_string = fs.readFileSync("library.json", "utf8");
    const new_library = JSON.parse(new_library_string);
    //console.log(new_library);
    return new_library;
}

function getBook(id: number) {

    const book_Array = (loadFromJSON());

    for (let i = 0; i < book_Array.length; i++) {
        if (book_Array[i].id == id) {
            return book_Array[i]
        }
    }
    return "Book not found";

}

function printBookData(id: number) {
    const data = getBook(id);
    console.log(data);

}

function printReadingStatus(author: string, title: string) {
    const book_Array = (loadFromJSON());

    for (let i = 0; i < book_Array.length; i++) {
        if (book_Array[i].author == author && book_Array[i].title == title) {
            return book_Array[i].readingStatus;
        }
    }
    return "Book not found";
}

function addNewBook(author: string, title: string) {
    const uusi = {
        author: author,
        title: title,
        readingStatus: false,
        id: library.length + 1
    }

    library.push(uusi)
}

function readBook(id: number) {
    //const book_Array = (loadFromJSON());

    for (let i = 0; i < library.length; i++) {
        if (library[i].readingStatus == false) {
            library[i].readingStatus = true;
            saveToJSON()
            return "Reading status changed succesfully"
        }
    }
    return "Book not found";
}

addNewBook("J.R.R. Tolkien", "Lord of the Rings")
addNewBook("tööt", "tööt")

saveToJSON()
loadFromJSON();

console.log(getBook(1));
console.log(getBook(4));

printBookData(4);
printBookData(5);

console.log(printReadingStatus("Douglas Hofstadter", "Gödel, Escher, Bach"));
console.log(library);
readBook(1);
console.log("muutos  tehty");
console.log(library);
console.log("json vielä");
console.log(loadFromJSON());
