const myArray = [1, 2, 3, 4, 5, 6]
const myArray2 = [1, 2, "toot", 4, 5, 6]
const myArray3 = [false, 2, true, 4, 5, 6]
const myArray4 = [0.1, 2, 3, 4, 5, checkType(myArray)]


function checkType(arr: Array<unknown>) {
    for (const a of arr) {
        if (typeof a === "number") {
            continue;
        }
        else return false
    }

    return true
}
//(a:Array<unknown>):boolean
console.log(checkType(myArray));
console.log(checkType(myArray2));
console.log(checkType(myArray3));
console.log(checkType(myArray4));