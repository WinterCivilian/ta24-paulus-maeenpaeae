import fs from "fs";
import { parse } from "path";

const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

const modifiedText = JSON.stringify(forecast);

const stream = fs.createWriteStream("./forecast_data.json");
stream.write(modifiedText, (err) => {
    if (err) console.log(err);
    else {
        console.log("success")
        const readStream = fs.createReadStream("./forecast_data.json", 'utf-8');
        readStream.on("data", (txt) => {
            console.log(txt);
            const parsed = JSON.parse(txt);
            parsed.temperature = 25;
            console.log(parsed.temperature);

            const updatedText = JSON.stringify(parsed);

            fs.writeFile("./forecast_data.json", updatedText, (err) => {
                if (err) console.log(err);
                else {
                    console.log("onnistui");
                }
            }
            );

        });
    }
});