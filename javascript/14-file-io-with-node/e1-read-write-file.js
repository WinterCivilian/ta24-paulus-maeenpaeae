import fs from "fs";

let textByWord = "";

const readStream = fs.createReadStream("./textFile.txt", 'utf-8');
readStream.on("data", (txt) => {
    textByWord = txt.split(/\s+/);
    for (let i = 0; i < textByWord.length; i++) {
        if (textByWord[i].toLowerCase() == "joulu") {
            textByWord[i] = "kinkku";
            console.log(textByWord[i]);
        }
        else if (textByWord[i].toLowerCase() == "lapsilla") {
            textByWord[i] = "poroilla";
            console.log(textByWord[i]);
        }
        else { console.log(textByWord[i]) }
    }

    const modifiedText = textByWord.join(' ');

    const stream = fs.createWriteStream("./newTextFile.txt");
    stream.write(modifiedText), (err) => {
        if (err) console.log(err);
        else console.log("success");
    };
});
