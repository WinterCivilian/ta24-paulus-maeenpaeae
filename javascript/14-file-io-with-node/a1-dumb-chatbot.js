import readline from "readline-sync";

let counter = 0;
let currentName = "Bob";

const cloudWeather = ["cloudy", "clear skies", "pitch black darkness"];
const sunWeather = ["sunny", "not sunny", "scorching"];
const windWeather = ["windy", "not windy", "hurricane"];

let answer = readline.question("Hi! I am a dumb chat bot You can check all the things I can do by typing 'help'.");

let talk = true;

function randomNumber() {
    return Math.floor(Math.random() * 3);
}


while (talk) {
    switch (answer) {
        case 'help':
            counter++;
            console.log(`-----------------------------
        Here´s a list of commands that I can execute! 
        
        help: Opens this dialog.
        hello: I will say hello to you
        botInfo: I will introduce myself
        botName: I will tell my name
        botRename: You can rename me
        forecast: I will forecast tomorrows weather 100 % accurately
        quit: Quits the program.
        -----------------------------`)
            answer = readline.question("How can I help?")
            break;
        case 'hello':
            counter++;
            const name = readline.question("What is your name?");
            answer = readline.question(`Hello ${name}.`);
            break;
        case 'botInfo':
            counter++;
            answer = readline.question(`I am a dumb bot. You can ask me almost anything :). You have already asked me ${counter} questions.`);
            break;
        case 'botName':
            counter++;
            answer = readline.question(`My name is currently ${currentName}. If you want to change it, type botRename.`);
            break;
        case 'botRename':
            counter++;
            let newName = readline.question(`Type my new name, please.`);
            let answerName = readline.question(`Are you happy with the name ${newName}.`);

            if (answerName == "yes") {
                currentName = newName;
                answer = readline.question(`I was renamed to ${currentName}.`)
                break;
            }
            else if (answerName == "no") {
                answer = readline.question(`Name not changed. My name is ${currentName}.`)
                break;
            }
            break;
        case 'forecast':
            counter++;
            answer = readline.question(`Tomorrows weather will be....
        Temperature: ${Math.floor(Math.random() * 30) + " Celsius"}
        Cloudy: ${cloudWeather[randomNumber()]}
        Sunny: ${sunWeather[randomNumber()]}
        Wind: ${windWeather[randomNumber()]} `);
            break;
        case 'quit':
            talk = false;
            console.log("Goodbye");
            break;
        default:
            answer = readline.question(`Sorry, I don't understand your command`);
    }
}