const axios = require("axios");

async function getUniversities() {
    const response = await axios.get(`http://universities.hipolabs.com/search?country=Finland`);
    return response.data
}


getUniversities().then(data => {

    const uniNames = data.map(entry => entry.name)
    console.log(uniNames);
});

