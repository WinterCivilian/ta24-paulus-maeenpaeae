const axios = require("axios");

async function getFakeStoreProducts() {
    const response = await axios.get(`https://fakestoreapi.com/products`);
    return response.data
}

getFakeStoreProducts().then(data => {

    const productNames = data.map(entry => entry.title)
    console.log(productNames);
});


//b)
function addFakeStoreProduct(name, price, description, category) {

    axios({
        method: `post`,
        url: `https://fakestoreapi.com/products`,
        data: {
            title: name,
            price: price,
            category: category,
            description: description
        },
    })
        .then((response) => {
            //console.log("HTTP Status Code:", response.status);
            console.log("The id of the new product: " + response.data.id);
        })
        .catch((error) => console.log(error));
}

addFakeStoreProduct("Shoes", 20, "Just some shoes", "clothes")


function deleteFakeStoreProduct(deleteID) {

    axios.delete(`https://fakestoreapi.com/products/${deleteID}`)
        .then(response => {
            console.log(`Deleted post with ID ${deleteID}`);
            console.log(response.data.title);
        })
        .catch(error => {
            console.error(error);
        });
}

deleteFakeStoreProduct(1);
deleteFakeStoreProduct(2);

