// Exercise 1 and 3
const axios = require("axios");

function getPostById(id) {

    axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then(response => console.log(`Post #${response.data.id}: ${response.data.title}: ${response.data.userId}.`),
            axios({
                method: `put`,
                url: `https://jsonplaceholder.typicode.com/todos/1`,
                data: {
                    id: 1,
                    userID: 1,
                    title: "Exercise 3",
                    completed: false
                },
            })
                .then((response) => {
                    console.log("HTTP Status Code:", response.status);
                    console.log(response.data);
                }))
        .catch(error => console.log(error));
}


async function getPostComments(id) {
    const axios = require("axios");
    const res = await axios.get(`https://jsonplaceholder.typicode.com/comments`, { params: { postId: id } });
    console.log(res);
}

getPostComments(5);


function post() {
    axios({
        method: `post`,
        url: `https://jsonplaceholder.typicode.com/todos`,
        data: {
            userID: 1,
            title: "Exercise 3",
            completed: false
        },
    })
        .then((response) => {
            console.log("HTTP Status Code:", response.status);
            console.log(response.data);
        })
        .catch((error) => console.log(error));
}

getPostById(1);

post();
