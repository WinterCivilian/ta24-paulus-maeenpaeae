let anotherFunction = function (arg: number, func: Function) {
    setTimeout(() => {
        func();
    }, arg);
}
anotherFunction(1000, () => {
    console.log("three");
    anotherFunction(1000, () => {
        console.log("two");
        anotherFunction(1000, () => {
            console.log("one");
            anotherFunction(1000, () => {
                console.log("GO!");
            })
        })
    })
});
