//a)
async function waitFor(milliseconds) {

    const promise = new Promise((resolve) => {
        setTimeout(() => resolve(), milliseconds)
    });
    await promise;
}

//waitFor(1000);

//b)
async function countSeconds() {

    for (let i = 0; i < 11; i++) {
        console.log(i);
        await waitFor(1000);
    }
}

countSeconds();