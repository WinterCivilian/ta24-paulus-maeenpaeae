let countDown = function (delay: number, message: string) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(message)
        }, delay)
    }

    )
}

countDown(1000, "Three").then(value => {
    console.log(value)
}).then(value => countDown(1000, "Two").then(value => console.log(value))
    .then(value => countDown(1000, "One").then(value => console.log(value)))
    .then(value => countDown(1000, "GO!").then(value => console.log(value))));
