
interface result {
    value?: number;
}

const getValue = function (): Promise<result> {
    return new Promise((res) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};


async function f() {

    const valueOneHere: result = await getValue();
    const valueTwoHere: result = await getValue();

    console.log(`Value 1 is ${valueOneHere.value} and value 2 is ${valueTwoHere.value}`);
}

f();

// Writing `Value 1 is ${valueOneHere.value}` doesn't work for some reason
getValue().then((value) => {
    console.log(value);
    return getValue();
}).then((value) => {
    console.log(value);
})


// Extra
Promise.all([getValue(), getValue()]).then((value) => {
    console.log(value);
})