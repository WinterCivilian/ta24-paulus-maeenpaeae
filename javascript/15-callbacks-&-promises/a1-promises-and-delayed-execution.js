//a)

function sum(limit) {
    let summa = 0;
    for (let i = 0; i < (limit + 1); i++) {
        summa += i;
    }

    return summa;
}

console.log(sum(10)); // prints 55 (= 0 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10)

//b)
const promiseA = new Promise((resolve) => {
    resolve(sum(50000));
}).then((result) => {
    console.log(result);
})


//c)

const promiseB = new Promise((resolve) => {
    setTimeout(() => {
        resolve(sum(50000));
    }, 2000);
}).then((result) => {
    console.log(result);
})

//d)

function createDelayedCalculation(limit, milliseconds) {

    const promiseC = new Promise((resolve) => {
        setTimeout(() => {
            resolve(sum(limit));
        }, milliseconds);
    })
    return promiseC;
}

// Prints 200000010000000 after a delay of 2 seconds
createDelayedCalculation(20000000, 2000).then(result => console.log(result));

// Prints 1250025000 after a delay of 0.5 seconds
createDelayedCalculation(50000, 500).then(result => console.log(result));

//e)
// The delay is 1.5 seconds bigger for createDelayedCalculation(20000000, 2000)