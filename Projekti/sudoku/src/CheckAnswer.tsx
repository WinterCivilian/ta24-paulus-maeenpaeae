function CheckAnswer(props) {

    function compareArrays(arr1, arr2) {
        // Check if arrays have the same length
        /*if (arr1.length !== arr2.length) {
            alert("Not the same length!");
        }*/

        /*for (let i = 0; i < arr1.length; i++) {
            console.log(arr2[i].value)
            if (arr2[i].value == "") {
                // If any element is not a number, show an alert and return false
                alert("Empty Squares");
                break;
            }
        }*/

        // Iterate over each element and compare
        for (let i = 0; i < arr1.length; i++) {
            //console.log(arr2[i].value)
            console.log(arr2.length)
            if (arr2[i].value == "") {
                // If any element is not a number, show an alert and return false
                alert("Empty Squares");
                break;
            }

            if (arr1[i] !== arr2[i]) {
                return false; // Arrays are not the same
            }
        }

        // If no differences found, arrays are the same
        return true;
    }

    const handleClick = () => {

        //korjaa Appissa numbers.values joksikin muuksi
        const isEqual = compareArrays(props.answer, props.numbers);
        console.log(props.numbers)
        if (isEqual) {
            alert("You win!");
        } else {
            alert("There are false answers!");
        }
    };

    return (
        <button onClick={handleClick}>Check answer</button>
    )
}

export default CheckAnswer;