import { useState } from 'react'
import './App.css'
import CheckAnswer from './CheckAnswer';

function App() {

  const [numbers, setNumbers] = useState([
    { value: 5, immutable: true }, { value: 3, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 7, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false },
    { value: 6, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 1, immutable: true }, { value: 9, immutable: true }, { value: 5, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false },
    { value: "", immutable: false }, { value: 9, immutable: true }, { value: 8, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 6, immutable: true }, { value: "", immutable: false },
    { value: 8, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 6, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 3, immutable: true },
    { value: 4, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 8, immutable: true }, { value: "", immutable: false }, { value: 3, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 1, immutable: true },
    { value: 7, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 2, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 6, immutable: true },
    { value: "", immutable: false }, { value: 6, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 2, immutable: true }, { value: 8, immutable: true }, { value: "", immutable: false },
    { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 4, immutable: true }, { value: 1, immutable: true }, { value: 9, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 5, immutable: true },
    { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 8, immutable: true }, { value: "", immutable: false }, { value: "", immutable: false }, { value: 7, immutable: true }, { value: 9, immutable: true }
  ]);

  const answer = [
    5, 3, 4, 6, 7, 8, 9, 1, 2,
    6, 7, 2, 1, 9, 5, 3, 4, 8,
    1, 9, 8, 3, 4, 2, 5, 6, 7,
    8, 5, 9, 7, 6, 1, 4, 2, 3,
    4, 2, 6, 8, 5, 3, 7, 9, 1,
    7, 1, 3, 9, 2, 4, 8, 5, 6,
    9, 6, 1, 5, 3, 7, 2, 8, 4,
    2, 8, 7, 4, 1, 9, 6, 3, 5,
    3, 4, 5, 2, 8, 6, 1, 7, 9
  ];

  return (
    <div className='sudoku'>
      {numbers.map((number, index) => {
        if (number.immutable) {
          return (
            <input
              key={index}
              id={`quantity-${index}`}
              className={'square'}
              value={number.value}
              disabled // Adding disabled attribute to make the input disabled
            />
          );
        } else {
          return (
            <input
              key={index}
              id={`quantity-${index}`}
              min="1"
              max="9"
              type='number'
              className={'square'}
              value={number.value}
              onChange={(e) => {
                const newValue = parseInt(e.target.value);
                const updatedNumbers = [...numbers];
                updatedNumbers[index].value = newValue;
                setNumbers(updatedNumbers);
              }}
            />
          );
        }
      })}
      <CheckAnswer numbers={numbers} answer={answer} />
    </div>
  );
}

export default App